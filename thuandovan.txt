doan@doan-5547:~/deep_learning$ ssh -X xteam@192.168.1.29
xteam@192.168.1.29's password: 
Welcome to Ubuntu 18.04.5 LTS (GNU/Linux 5.4.0-77-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

7 updates can be applied immediately.
2 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable

New release '20.04.2 LTS' available.
Run 'do-release-upgrade' to upgrade to it.

Your Hardware Enablement Stack (HWE) is supported until April 2023.
Last login: Tue Jul  6 14:44:26 2021 from 192.168.1.6
xteam@xteam-desktop:~$ cd Documents/Thuan/
xteam@xteam-desktop:~/Documents/Thuan$ ls
convert_yolo2wts        face-alignment       retinafacemask          triton_deepstream
deepsort+yolov4         Face-Mask-Detection  test_triton_deepstream  TritonInferenceServer
deepstream_python_apps  insightface          test_triton_pytorch     triton_inference_server_2
xteam@xteam-desktop:~/Documents/Thuan$ cd insightface/
xteam@xteam-desktop:~/Documents/Thuan/insightface$ cd de
deploy/    detection/ 
xteam@xteam-desktop:~/Documents/Thuan/insightface$ cd de
deploy/    detection/ 
xteam@xteam-desktop:~/Documents/Thuan/insightface$ cd detection/
xteam@xteam-desktop:~/Documents/Thuan/insightface/detection$ ls
data_mask  label  RetinaFace  RetinaFaceAntiCov  retinaface_gt_v1.1  scrfd
xteam@xteam-desktop:~/Documents/Thuan/insightface/detection$ dc RetinaFaceAntiCov/
dc: Will not attempt to process directory RetinaFaceAntiCov/
xteam@xteam-desktop:~/Documents/Thuan/insightface/detection$ ls
data_mask  label  RetinaFace  RetinaFaceAntiCov  retinaface_gt_v1.1  scrfd
xteam@xteam-desktop:~/Documents/Thuan/insightface/detection$ cd RetinaFaceAntiCov/
xteam@xteam-desktop:~/Documents/Thuan/insightface/detection/RetinaFaceAntiCov$ ls
abc.jpg         build_eval_pack.py  face.jpg   medical-masks-dataset  rcnn               test.py
align_mask.zip  cov_test.jpg        Makefile   model                  README.md          test_with_mask
anno_mask       face_align.py       mask.jpeg  __pycache__            retinaface_cov.py  test_without_mask
xteam@xteam-desktop:~/Documents/Thuan/insightface/detection/RetinaFaceAntiCov$ nano build_eval_pack.py 
Use "fg" to return to nano.

[1]+  Stopped                 nano build_eval_pack.py
xteam@xteam-desktop:~/Documents/Thuan/insightface/detection/RetinaFaceAntiCov$ ls
abc.jpg         build_eval_pack.py  face.jpg   medical-masks-dataset  rcnn               test.py
align_mask.zip  cov_test.jpg        Makefile   model                  README.md          test_with_mask
anno_mask       face_align.py       mask.jpeg  __pycache__            retinaface_cov.py  test_without_mask
xteam@xteam-desktop:~/Documents/Thuan/insightface/detection/RetinaFaceAntiCov$ 
